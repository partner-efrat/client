import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from "./app.component";

// import { MAIN_ROUTES } from "./+main";
import {AUTH_ROUTES} from "./+auth";
import { HomeComponent } from './+main/home/home.component';
import { LoginComponent } from './+main/login/login.component';
import { AddUserComponent } from './+main/add-user/add-user.component';
import { MainComponent } from './+main/main.component';
import { NewProjectComponent } from './+main/new-project/new-project.component';
import { DetailsProjectComponent } from './details-project/details-project.component';

// export const APP_ROUTES: Routes = [
//     {
//         path:'home',
//         component:AppComponent,
       
//     },
//     { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },

//     ...AUTH_ROUTES, 
//     ...MAIN_ROUTES    
// ];
 export const APP_ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent,
        
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'add',
        component: AddUserComponent
    },

    {

        path: 'main',
        component: MainComponent,
       
    },
    {

        path: 'newProject',
        component: NewProjectComponent,
       
    },
    {

        path: 'detailsProject',
        component: DetailsProjectComponent,
       
    }
]

  

@NgModule({
    imports: [
        RouterModule.forRoot(APP_ROUTES/*, { useHash: true }*/)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }