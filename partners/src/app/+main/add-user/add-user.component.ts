import { Component, OnInit } from '@angular/core';
import { ContactsDTO } from '../../models/dto/ContactsDTO';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
user:ContactsDTO;
  constructor() { }

  ngOnInit() {
    this.user=new ContactsDTO();
  }

}
