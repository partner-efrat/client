import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent  implements OnInit {
  menu:MenuItem[];

  ngOnInit() {
    this.menu=[
      {},
      {label:'אזור אישי',url:'personal-area'},
      {label:'צפיה במיזמים',url:'projects'},
      {label:'מיזם חדש',url:'new-project'},
      {label:'בית',url:'home'},
      {label:'הזדהות',url:'login'}
    ]
  }
}
