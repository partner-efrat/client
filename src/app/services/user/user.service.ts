import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/AppSettings';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {  ContactsDto } from 'src/app/Models/dto/Contact';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
};


const ApiBaseRoute = AppSettings.API_ENDPOINT+ "User/" ;

@Injectable({
  providedIn: 'root'
})
export class UserService {




  
  constructor( private http: HttpClient) { }


  public addUser(newUser : ContactsDto):Observable<boolean>{
    debugger;
  return this.http.get<boolean>(ApiBaseRoute + "AddUser?name="+newUser.name+"&mail="+newUser.mail+"&img="+newUser.img);

}


}
