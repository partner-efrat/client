import { TestBed } from '@angular/core/testing';

import { ExchangingService } from './exchanging.service';

describe('ExchangingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExchangingService = TestBed.get(ExchangingService);
    expect(service).toBeTruthy();
  });
});
