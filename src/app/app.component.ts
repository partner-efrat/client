import { Component, OnChanges, DoCheck } from '@angular/core';
import { AuthService } from './core/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

// import { ConstraintService } from './services/Constraint/constraint.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']

})
export class AppComponent implements DoCheck  {
 
  title = 'partner';
  iduser: string;
  profil;
  name;
  provider;
  isLogin :boolean= false;

  
  loader: boolean;
  url: string;
  loginFlag: boolean=false;

  constructor(
    public authService: AuthService,
    private r: Router,
   // private constraintService: ConstraintService

  ) {
    // this.constraintService.changeEmitted.subscribe(
    //   text => {
    //      this.getConstraiint();
    //   }); 
  }

  ngDoCheck(){
    this.url = document.URL.split("/")[3];
    this.iduser = JSON.parse(localStorage.getItem('idUser'));
    //this.teacher = JSON.parse(localStorage.getItem("teacher"));
    let a=JSON.parse(localStorage.getItem('idUser'));
    if(this.url!="login"&&this.url!="register"&&this.url!="user"&&this.loginFlag==false)
    {
    //  this.getConstraiint();
      this.loginFlag=true;
    }
    if(a)
    {
      this.isLogin = true;
    }
    this.profil = JSON.parse(localStorage.getItem('profil'));
    this.provider = JSON.parse(localStorage.getItem('provider'));
    let x = JSON.parse(localStorage.getItem('teacher'))
    if(x){
      this.name = x.FirstName + " " + x.LastName;
    }
  }
  ngOnInit() {
    
    console.log("this.isLogin", this.isLogin);
    
    }

 
  editConstraint() {
    this.r.navigateByUrl("/edit-constraint");
  }
  logout() {
    localStorage.clear();
    this.isLogin = false;
    this.iduser = null;
    this.profil = null;
    this.provider = null;
    this.loginFlag=false;
    this.authService.doLogout()
      .then((res) => {

        this.r.navigate(["/login"])

      }, (error) => {
        console.log("Logout error", error);
        this.r.navigate(["/login"])
      });
  }
  register(){
    this.r.navigate(["/login"])
  }
  projects(){
    this.r.navigate(["/projects"])
  }


  // getConstraiint(){
  //   this.constraintService.getAddressConstraints(this.teacher.Id).subscribe(
  //     (data: AddressConstraintDto[]) => {
  //       this.constraintService.addressConstraint = data;
  //       this.AddressConstraint = data;
  //       console.log("address constraint", this.AddressConstraint);
  //     },
  //     fail => alert("שגיאה בשרת"));

  //   this.constraintService.getDayConstraints(this.teacher.Id).subscribe(
  //     (data: DayConstraintDto[]) => {

  //       this.constraintService.dayConstraint = data;
  //       this.DayConstraint = data;
  //       console.log("day constraint", this.DayConstraint);
  //     },
  //     fail => alert("שגיאה בשרת"));

  //   this.constraintService.getAgeConstraints(this.teacher.Id).subscribe(
  //     (data: AgeConstraintDto[]) => {
  //       this.constraintService.ageConstraint = data;
  //       this.AgeConstraint = data;
  //       console.log("age constraint", this.AgeConstraint);
  //     },
  //     fail => alert("שגיאה בשרת"));

  //   this.constraintService.getGenderConstraints(this.teacher.Id).subscribe(
  //     (data: GenderConstraintDto[]) => {
  //       this.constraintService.genderConstraint = data;
  //       this.GenderConstraint = data;
  //       console.log("gender constraint", this.GenderConstraint);
  //     },
  //     fail => alert("שגיאה בשרת"));

  // }
  
}