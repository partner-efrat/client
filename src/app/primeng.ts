
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {DialogModule} from 'primeng/dialog'
import { BrowserModule } from '@angular/platform-browser';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { DataViewModule } from 'primeng/dataview';
import { CheckboxModule } from 'primeng/checkbox';
import { ChartModule, CalendarModule, SharedModule, PanelModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { MultiSelectModule } from 'primeng/multiselect';
import { KeyFilterModule } from 'primeng/keyfilter';
import {
  MenuModule, ContextMenuModule, InputTextModule, StepsModule, SpinnerModule, DataTableModule, TabMenuModule, FileUploadModule, RadioButtonModule, InputMaskModule,
  ProgressSpinnerModule, LightboxModule
}
  from 'primeng/primeng';
import { CardModule } from 'primeng/card';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
// import {ConfirmDialogModule} from 'primeng/confirmdialog';
// import {ConfirmationService} from 'primeng/api';
@NgModule({
  declarations: [
     
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AccordionModule,
    ButtonModule,
    DataViewModule,
    CheckboxModule,
    ChartModule,
    CalendarModule,
    SharedModule,
    PanelModule,
    DropdownModule,
    MultiSelectModule,
    KeyFilterModule,
    MenuModule,
    ContextMenuModule,
    InputTextModule,
    StepsModule,
    SpinnerModule,
    DataTableModule,
    TabMenuModule,
    FileUploadModule,
    RadioButtonModule,
    InputMaskModule,
    ProgressSpinnerModule,
    LightboxModule,
    CardModule,
    ScrollPanelModule,
    FormsModule,
    RouterModule,
   
    HttpModule,
    DialogModule,
    // ConfirmDialogModule,
    // ConfirmationService
  ],


  exports: [
    BrowserModule,
    BrowserAnimationsModule,
    AccordionModule,
    ButtonModule,
    DataViewModule,
    CheckboxModule,
    ChartModule,
    CalendarModule,
    SharedModule,
    PanelModule,
    DropdownModule,
    MultiSelectModule,
    KeyFilterModule,
    MenuModule,
    ContextMenuModule,
    InputTextModule,
    StepsModule,
    SpinnerModule,
    DataTableModule,
    TabMenuModule,
    FileUploadModule,
    RadioButtonModule,
    InputMaskModule,
    ProgressSpinnerModule,
    LightboxModule,
    CardModule,
    HttpModule,
    FormsModule,
    RouterModule,
    ScrollPanelModule,
    DialogModule,
    // ConfirmDialogModule,
    // ConfirmationService,
  ],
})
export class PrimengModule { }