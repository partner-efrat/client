
import { LoginComponent } from './component/login/login.component';
import { UserComponent } from './component/user/user.component';
import { RegisterComponent } from './component/register/register.component';
import { UserResolver } from './component/user/user.resolver';
import { AuthGuard } from './core/auth.guard';
import { Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { DetailsProjectComponent } from 'partners/src/app/details-project/details-project.component';
import { DetailsComponent } from './component/details/details.component';
import { ProjectsComponent } from './component/projects/projects.component';




export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'projects', component: ProjectsComponent },
  { path: 'login', component: LoginComponent,canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard]  },
  { path: 'user', component: UserComponent,  resolve: { data: UserResolver} },
  { path:'home', component:HomeComponent, },
  { path:'details', component:DetailsComponent, },
  { path:'details/:id', component:DetailsComponent, }

  // {path:'details/:id', component: DetailsProjectComponent,},
  // {path:'details', component: DetailsProjectComponent,},

  // {path:'search/:date',component:SearchComponent},


];