import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../core/auth.service'
import { Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Menu, MenuItem } from 'primeng/primeng';
import { componentRefresh } from '@angular/core/src/render3/instructions';
import { LoginService } from 'src/app/services/Auth';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss','../register/register.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;
  errorMessage: string = '';
  loginSucceed = true;
  constructor(
    public authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private loginService:LoginService,
    private userService:UserService


  ) {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['',Validators.required]
    });
  }


  tryGoogleLogin(){
    this.authService.doGoogleLogin()
    .then(res => {

     
     window.location.reload();
       
      this.router.navigate(['/user']);
    })
  }

  tryLogin(value){
    this.authService.doLogin(value)
    .then(res => {
      this.router.navigate(['/user']);
    }, err => {
      console.log(err);
      this.loginService.errorMessage ="אין רשומת משתמש המתאימה למזהה זה. באפשרותך להירשם.";
      
    })
  }

}
