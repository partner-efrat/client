import { Component, OnInit, HostListener, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseUserModel } from '../../core/user.model';
import { LoginService } from 'src/app/services/Auth/login.service';
import { UserService } from 'src/app/services/user/user.service';
import { ContactsDto } from 'src/app/Models/dto/Contact';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: FirebaseUserModel = new FirebaseUserModel();
  profileForm: FormGroup;
  newUser:ContactsDto= new ContactsDto();
  @Output() OnLogin = new EventEmitter();
  isLogin: string;
  flag = 0;
  u = null;
  email: string;

  constructor(
    // public userService: UserService,
    public authService: AuthService,
    public loginService: LoginService,
    private route: ActivatedRoute,
    private r: Router,
    private router: Router,
    public userService: UserService,

    private fb: FormBuilder,

  ) {
  }
  ngOnInit(): void {
    // this.loginService.getTeacherByEmail().subscribe(
    //   teacher => {
    //     if (teacher.Id == "0") {
    //       localStorage.clear();
    //       this.isLogin = null;
    //       this.authService.doLogout()
    //         .then((res) => {
    //           this.loginService.errorMessage = "אין לך הרשאת כניסה למערכת הגנים"
    //           this.r.navigate(["/login"])

    //         }, (error) => {
    //           console.log("Logout error", error);
    //           this.r.navigate(["/login"])
    //         });
    //     }
    //     else {
    //       this.teacher = teacher
    //       localStorage.setItem('teacher', JSON.stringify(this.teacher));
    //       this.OnLogin.emit();
    //       console.log("teacher", this.teacher);
    //       this.r.navigate(["/home"])

    //     }
    //   },
    //   err => console.error('Observer got an error: ' + err));

    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.user = data;
        this.createForm(this.user.name);
        this.email = JSON.parse(localStorage.getItem('email'));
        localStorage.setItem('profil', JSON.stringify(this.user.image));
        localStorage.setItem('name', JSON.stringify(this.user.name));
        localStorage.setItem('provider', JSON.stringify(this.user.provider));
        this.newUser.name=this.user.name;
        this.newUser.mail=this.email;
       this.newUser.img=this.user.image;

debugger;
        this.userService.addUser(this.newUser).subscribe(
          user => { 
            
            
              err => console.error('tttttttttttt' + err)
  
          });
          // this.userService.addUser2().subscribe(
          //   user => { 
              
              
          //       err => console.error('tttttttttttt' + err)
    
          //   });
        this.router.navigate(['/home']);


      }
    })
  }
  

  createForm(name) {
    this.profileForm = this.fb.group({
      name: [name, Validators.required]
    });
  }


}
