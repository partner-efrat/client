import { Component, OnInit } from '@angular/core';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {MenuItem} from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private r: Router,


  ) { }
menu:MenuItem[];
  ngOnInit() {
    this.menu=[
      {label:'אזור אישי',url:'personal-area'},
      {label:'צפיה במיזמים',url:'projects'},
      {label:'מיזם חדש',url:'new-project'},
      {label:'בית',url:'home'},
      {label:'הזדהות',url:'login'}
    ]
  }
  open(id){
    this.r.navigate(["details" + id])
debugger;

  }
}
