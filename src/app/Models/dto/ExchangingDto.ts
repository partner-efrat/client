export class ExchangingDto {
    Id: number
    ClassId: number
    ClassName:string
    SourceDate: string
    SourceDayName:string
    TeacherId: number
    TeacherName:string
    SubstituteTeacherId: number
    SubstituteTeacherName:string
    DestinationDate: Date
    DestinationDayName:string
    IsExchanging:boolean
    Message:string
    ClassAddress:string
    AskId:number
    IsRelevant:boolean
}
