
export const environment = {
  production: false,
  
	apiServer: {
		port: 1234,
		useHttps: false,
		serverUrl: 'localhost',
		fullAddress: function() {
			let protocol = environment.apiServer.useHttps ? 'https' : 'http';
			let address = environment.apiServer.serverUrl;
			let port = environment.apiServer.port && environment.apiServer.port > 0 ? `:${environment.apiServer.port}` : '';
	
			return `${protocol}://${address}${port}/`;
		}
	},
  firebase: {
  apiKey: "AIzaSyBKOQ-2aNbJ19J4ka2qSI-tKBNJoyIhDlw",
  authDomain: "partner-274d3.firebaseapp.com",
  databaseURL: "https://partner-274d3.firebaseio.com",
  projectId: "partner-274d3",
  storageBucket: "partner-274d3.appspot.com",
  messagingSenderId: "841817778496",
  appId: "1:841817778496:web:e68bdbef10b4b99989b2a5",
  measurementId: "G-NC31SB150K"
   

  }

  
};


